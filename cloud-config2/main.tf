# Passing Terraform variables onto the cloud-config.yaml file

data "template_file" "userdata2" {
  template = "${file("cloud-config-2.yaml")}"

  vars {
    db_endpoint     = "${aws_db_instance.name.address}" # Example Var
  }
}

# Main Userdata with some random vafs and a userdata_2 vars
data "template_file" "userdata" {
  template = "${file("cloud-config.yaml")}"

  vars {
    db_endpoint            = "${aws_db_instance.service.address}"
    environment_tag        = "${var.environment_tag}"
    external_protocol      = "${var.external_protocol}"
    service_image          = "${var.service_image_repo}:${var.service_image_tag}"
    userdata_s3_bucket     = "${aws_s3_bucket.service_userdata.id}"
    userdata_s3_object     = "${aws_s3_bucket_object.service_userdata.id}"
    userdata_2_sha256      = "${sha256(data.template_file.userdata2.rendered)}"
  }
}

resource "aws_launch_configuration" "service_app" {
  name_prefix          = "${module.naming.aws_launch_configuration}"
  image_id             = "${module.ami.ami_id}"
  instance_type        = "${var.instance_type}"
  user_data            = "${data.template_file.userdata.rendered}"
  key_name             = "${var.ssh_key}"
  iam_instance_profile = "${aws_iam_instance_profile.service_instance_profile.name}"
  enable_monitoring    = "${var.enable_monitoring}"

  root_block_device {
    volume_size = "${var.root_volume_size}"
  }

  security_groups = "${module.security_groups.sg_lookup[format("%s.%s", var.vpc, "app")]}"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_s3_bucket" "service_userdata" {
  bucket = "${module.naming.aws_s3_bucket}-userdata"
  acl    = "private"

  policy = <<POLICY
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "${aws_iam_role.service_iam_instance_role.arn}"
        ]
      },
      "Action": [
        "s3:List*",
        "s3:Put*",
        "s3:Get*"
      ],
      "Resource": "arn:aws:s3:::${module.naming.aws_s3_bucket}-userdata"
    },
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "${aws_iam_role.service_iam_instance_role.arn}"
        ]
      },
      "Action": [
        "s3:List*",
        "s3:Put*",
        "s3:Get*"
      ],
      "Resource": "arn:aws:s3:::${module.naming.aws_s3_bucket}-userdata/*"
    }
  ]
}
POLICY

  tags {
    "InventoryCode" = "${var.inventory_code_tag}"
    "ProductCode"   = "${var.product_code_tag}"
    "Environment"   = "${var.environment_tag}"
  }

  force_destroy = false

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_iam_policy" "S3UserDataBucketFullAccess" {
  name        = "${aws_s3_bucket.service_userdata.id}-UserDataS3BucketFullAccess"
  path        = "/"
  description = "Allow full access to the product s3 bucket ${aws_s3_bucket.service_userdata.arn}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": ["s3:ListBucket"],
      "Resource": "${aws_s3_bucket.service_userdata.arn}"
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:PutObject",
        "s3:GetObject",
        "s3:DeleteObject"
      ],
      "Resource": [
        "${aws_s3_bucket.service_userdata.arn}/*"
      ]
    }
  ]
}
EOF
}

resource "aws_s3_bucket_object" "service_userdata" {
  key     = "userdata"
  bucket  = "${aws_s3_bucket.service_userdata.bucket}"
  content = "${data.template_file.userdata2.rendered}"
}
